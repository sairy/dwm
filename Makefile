# dwm - dynamic window manager
# See LICENSE file for copyright and license details.

include config.mk

SRC = drw.c dwm.c util.c
OBJ = ${SRC:.c=.o}

all: options dwm

options:
	@echo dwm build options:
	@echo "CFLAGS   = ${CFLAGS}"
	@echo "LDFLAGS  = ${LDFLAGS}"
	@echo "CC       = ${CC}"
	@echo

.c.o:
	${CC} -c ${CFLAGS} $<

${OBJ}: config.h config.mk

config.h:
	cp config.def.h $@

dwm: ${OBJ}
	${CC} -o $@ ${OBJ} ${LDFLAGS}

clean:
	rm -f dwm ${OBJ} dwm-mira-${VERSION}.tar.gz
	rm -f *.{orig,rej}

dist:
	mkdir -p dwm-mira-${VERSION}
	cp -R LICENSE Makefile README config.mk\
		dwm.1 drw.h util.h ${SRC} dwm.png transient.c\
		patches scripts dwm.desktop dwm-mira-${VERSION}
	cp config.h dwm-mira-${VERSION}/config.def.h
	tar -cf dwm-mira-${VERSION}.tar dwm-mira-${VERSION}
	gzip -f dwm-mira-${VERSION}.tar
	rm -rf dwm-mira-${VERSION}

install: all
	mkdir -p ${DESTDIR}${PREFIX}/bin
	cp -f dwm ${DESTDIR}${PREFIX}/bin
	chmod 755 ${DESTDIR}${PREFIX}/bin/dwm
	mkdir -p ${DESTDIR}${MANPREFIX}/man1
	sed "s/VERSION/${VERSION}/g" < dwm.1 > ${DESTDIR}${MANPREFIX}/man1/dwm.1
	chmod 644 ${DESTDIR}${MANPREFIX}/man1/dwm.1
	mkdir -p ${DESTDIR}/usr/share/xsessions
	cp -f dwm.desktop ${DESTDIR}/usr/share/xsessions

uninstall:
	rm -f ${DESTDIR}${PREFIX}/bin/dwm\
		${DESTDIR}${MANPREFIX}/man1/dwm.1\
		${DESTDIR}/usr/share/xsessions/dwm.desktop

.PHONY: all options clean dist install uninstall
