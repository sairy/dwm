/* See LICENSE file for copyright and license details. */

/* autostart */
static const char autostartblocksh[] = "autostart_blocking.sh";
static const char autostartsh[] = "autostart.sh";

/* appearance */
static const Gap default_gap        = { .isgap = 1, .realgap = 10, .gappx = 10 };
static const unsigned int borderpx  = 2;        /* border pixel of windows */
static const unsigned int snap      = 10;       /* snap pixel */

static const int scalepreview       = 4;        /* tag preview scaling */
static const int swallowfloating    = 0;        /* 1 means swallow floating windows by default */

static const char buttonbar[]       = "";      /* icon to display next to the tags */
static const int buttonpad          = +4;       /* right padding for the button */

static const int showbar            = 1;        /* 0 means no bar */
static const int topbar             = 1;        /* 0 means bottom bar */
static const int startontag         = 0;        /* 0 means no tag active on start */

static const int vertpad            = 10;       /* vertical padding of bar */
static const int sidepad            = 10;       /* horizontal padding of bar */

/* Helper for adding new fonts */
#define FONT(FNAME, SIZE) FNAME":pixelsize="#SIZE":antialias=true:autohint=true"

static const char *fonts[] = {
    /* main font */
    FONT("Monaco", 16),
    /* icons */
    FONT("FiraCode Nerd Font:style=Retina", 16),
    /* 漢字 */
    FONT("Source Han Code JP:style=R", 16),
    /* emoji */
    FONT("Twemoji", 12),
};

static const char dmenufont[]   = FONT("Monaco", 16);

static const char col_gray1[]   = "#161320"; /* bg color */
static const char col_gray2[]   = "#595959"; /* inactive window border color */
static const char col_gray3[]   = "#F5C2E7"; /* font color */
static const char col_gray4[]   = "#1A1826"; /* font color for current window */
static const char col_accent[]  = "#F5C2E7"; /* top bar and window border color */

static const char *colors[][3]  = {
    /*               fg         bg			border		*/
    [SchemeNorm] = { col_gray3, col_gray1,	col_gray2	},
    [SchemeSel]  = { col_gray4, col_accent,	col_accent	},
};

/* tagging */
static const char *tags[] = { "一", "二", "三", "四", "五", "六", "七", "八", "九" };

static const Rule rules[] = {
    /* xprop(1):
     *	WM_CLASS(STRING) = instance, class
     *	WM_NAME(STRING)	 = title
     */
    /* class            instance    title       tags mask       isfloating  isterminal  noswallow   monitor */
    { "st-256color",    NULL,       NULL,       0,              0,          1,          0,          0 },
    { "kitty",          NULL,       NULL,       0,              0,          1,          0,          0 },
    {  NULL,            NULL,  "Event Tester",  0,              0,          0,          1,         -1 }, /* xev */
    { "firefox",        NULL,       NULL,       1 << 1,         0,          0,          1,         -1 },
    { "LibreWolf",      NULL,       NULL,       1 << 1,         0,          0,          1,          0 },
    { "krita",          NULL,       NULL,       1 << 7,         0,          0,          0,         -1 },
    { "mpv",            NULL,       NULL,       0,              1,          0,          0,         -1 },
    { "KeePassXC",      NULL,       NULL,       0,              1,          0,          0,          1 },
    { "MEGAsync",       NULL,       NULL,       0,              1,          0,          0,         -1 },
    { "Nsxiv",          NULL,       NULL,       0,              1,          0,          1,          0 },
    { "kcalc",          NULL,       NULL,       0,              1,          0,          0,         -1 },
    /* { "Thunderbird",    NULL,       NULL,       1 << 8,         0,          0,           0,          0 }, */
    { "firefox",    NULL, "Picture-in-Picture", 0,              1,          0,          1,         -1 },
};

/* layout(s) */
static const float mfact     = 0.55; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 1;    /* 1 means respect size hints in tiled resizals */

static const Layout layouts[] = {
    /* symbol   arrange function */
    { " ",     tile },    /* first entry is default */
    { " ",     NULL },    /* no layout function means floating behavior */
    { " ",     monocle },
};

/* key definitions
 *
 * Mod1 = Alt
 * Mod2 = NumLock
 * Mod3 = idk ¯\_(ツ)_/¯
 * Mod4 = Super/Meta
 * Mod5 = AltGr
 */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = {
    "dmenu_run", "-m", dmenumon,
    "-p", "Launch: ", "-fn", dmenufont,
    "-nb", col_gray1, "-nf", col_gray3,
    "-sb", col_accent, "-sf", col_gray4,
    "-x", "10" , "-y", "10", "-z", "1898",
    topbar ? NULL : "-b", NULL
};

static const char *termcmd[]  = { "st", "-A", "0.8", NULL };

static const char *browser[]  = { "firefox",   NULL };
static const char *torcmd[]   = { "tor-browser", NULL };

static const char *lockcmd[]  = { "slock", NULL };

static const char *scrsele[]  = { "/bin/sh", "-c", "scrot -s -zl color='#c2ff73',opacity=255,mode=edge -F 'Screenshot_%Y%m%d_%H%M%S.png' -e 'xclip -t image/png $f; [ ! -f ~/pics/screenshots/$f ] && mv $f ~/pics/screenshots/' && herbe 'Screenshot taken!' && dolphin ~/pics/screenshots", NULL };
static const char *scrfull[]  = { "/bin/sh", "-c", "scrot -zl color='#c2ff73',opacity=255,mode=edge -F 'Screenshot_%Y%m%d_%H%M%S.png' -e 'xclip -t image/png $f; [ ! -f ~/pics/screenshots/$f ] && mv $f ~/pics/screenshots/' && herbe 'Full screenshot taken!' && dolphin ~/pics/screenshots", NULL };



/* Fn Keys */
static const char *upvol[]    = { "/bin/sh", "-c", "pamixer -i 2; pkill -RTMIN+2 dwmblocks",    NULL };
static const char *downvol[]  = { "/bin/sh", "-c", "pamixer -d 2; pkill -RTMIN+2 dwmblocks",    NULL };
static const char *mute[]     = { "/bin/sh", "-c", "pamixer -t; pkill -RTMIN+2 dwmblocks",      NULL };
static const char *upmic[]    = { "pamixer", "--source", "@DEFAULT_SOURCE@", "-i", "2",         NULL };
static const char *downmic[]  = { "pamixer", "--source", "@DEFAULT_SOURCE@", "-d", "2",         NULL };
static const char *mutemic[]  = { "/bin/sh", "-c", "pamixer --source @DEFAULT_SOURCE@ -t; pkill -RTMIN+2 dwmblocks", NULL };
static const char *dnbright[] = { "light", "-U", "5",      NULL };
static const char *upbright[] = { "light", "-A", "5",      NULL };
static const char *calccmd[]  = { "kcalc",                 NULL };



/* Media Keys */
static const char *playpause[] = { "playerctl", "play-pause",  NULL };
static const char *stopplay[]  = { "playerctl", "stop",        NULL };
static const char *playnext[]  = { "playerctl", "next",        NULL };
static const char *playprev[]  = { "playerctl", "previous",    NULL };


static const Key keys[] = {
	/* modifier                     key        function        argument */
	{ MODKEY|ShiftMask,             XK_q,      quit,           {0} }, /* quit */
	{ MODKEY|ControlMask,           XK_q,      quit,           {1} }, /* restart */
	{ MODKEY|Mod1Mask,              XK_q,      spawn,          SHCMD("~/.config/dwm/wmsh") },

	/* Window management */
	{ MODKEY,                       XK_q,      killclient,     {0} },
	{ MODKEY,                       XK_j,      focusstack,     {.i = +1} },
	{ MODKEY,                       XK_k,      focusstack,     {.i = -1} },
	{ MODKEY|ShiftMask,             XK_j,      movestack,      {.i = +1} },
	{ MODKEY|ShiftMask,             XK_k,      movestack,      {.i = -1} },
	{ MODKEY,                       XK_h,      setmfact,       {.f = -0.05} },
	{ MODKEY,                       XK_l,      setmfact,       {.f = +0.05} },
	{ MODKEY,                       XK_Return, zoom,           {0} },
	{ MODKEY,                       XK_Tab,    view,           {0} },
	{ MODKEY,                       XK_0,      view,           {.ui = ~0} },
	{ MODKEY|ShiftMask,             XK_0,      tag,            {.ui = ~0} },
	{ MODKEY,                       XK_comma,  focusmon,       {.i = -1} },
	{ MODKEY,                       XK_period, focusmon,       {.i = +1} },
	{ MODKEY|ShiftMask,             XK_comma,  tagmon,         {.i = -1} },
	{ MODKEY|ShiftMask,             XK_period, tagmon,         {.i = +1} },
	{ MODKEY,                       XK_i,      incnmaster,     {.i = +1} },
	{ MODKEY,                       XK_d,      incnmaster,     {.i = -1} },

	/* Layout management */
	{ MODKEY,                       XK_b,      togglebar,      {0} },
	{ MODKEY,                       XK_t,      setlayout,      {.v = &layouts[0]} },
	{ MODKEY,                       XK_f,      setlayout,      {.v = &layouts[1]} },
	{ MODKEY,                       XK_m,      setlayout,      {.v = &layouts[2]} },
	{ MODKEY,                       XK_c,      centerclient,   {0} },
	{ MODKEY,                       XK_space,  togglefloating, {0} },
	{ MODKEY|ShiftMask,             XK_space,  setlayout,	   {0} },


	/* Gaps management */
	{ MODKEY,                       XK_minus,  setgaps,        {.i = -1} },
	{ MODKEY|ShiftMask,             XK_equal,  setgaps,        {.i = +1} },
	{ MODKEY,                       XK_equal,  setgaps,        {.i = GAP_RESET } },
	{ MODKEY|ShiftMask,             XK_minus,  setgaps,        {.i = GAP_TOGGLE} },


	/* Spawn commands */
	{ MODKEY|ShiftMask,             XK_Return, spawn,          {.v = termcmd } },

	{ MODKEY,                       XK_p,      spawn,          {.v = dmenucmd} },
	{ MODKEY|ShiftMask,             XK_p,      spawn,          SHCMD("~/.local/bin/dmenukaomoji") },
	{ MODKEY|ShiftMask,             XK_n,      spawn,          SHCMD("networkmanager_dmenu") },
	{ MODKEY,                       XK_o,      spawn,          {.v = browser } },
	{ MODKEY|ShiftMask,             XK_o,      spawn,          {.v = torcmd  } },
	{ MODKEY|ShiftMask,             XK_l,      spawn,          {.v = lockcmd } },

	{ Mod1Mask,                    XK_Shift_L, spawn,          SHCMD("~/.config/dwm/kblayout") },
	/* { MODKEY|ShiftMask,          XK_BackSpace, spawn,          SHCMD("/.config/dwm/toggle-touchpad") }, */

	{ MODKEY,                       XK_Print,  spawn,          {.v = scrsele } },
	{ MODKEY|ShiftMask,             XK_Print,  spawn,          {.v = scrfull } },


	/* Fn Keys */
	{ 0,             XF86XK_AudioMute,         spawn,          {.v = mute    } },
	{ 0,             XF86XK_AudioLowerVolume,  spawn,          {.v = downvol } },
	{ 0,             XF86XK_AudioRaiseVolume,  spawn,          {.v = upvol   } },
	{ 0|ShiftMask,   XF86XK_AudioLowerVolume,  spawn,          {.v = downmic } },
	{ 0|ShiftMask,   XF86XK_AudioRaiseVolume,  spawn,          {.v = upmic   } },
	{ 0,             XF86XK_AudioMicMute,      spawn,          {.v = mutemic } },
	{ 0,             XF86XK_MonBrightnessDown, spawn,          {.v = dnbright} },
	{ 0,             XF86XK_MonBrightnessUp,   spawn,          {.v = upbright} },
	{ 0,             XF86XK_Calculator,        spawn,          {.v = calccmd } },

	/* Media Keys */
	{ 0,                    XF86XK_AudioPlay,  spawn,          {.v = playpause} },
	{ 0,                    XF86XK_AudioStop,  spawn,          {.v = stopplay } },
	{ 0,                    XF86XK_AudioNext,  spawn,          {.v = playnext } },
	{ 0,                    XF86XK_AudioPrev,  spawn,          {.v = playprev } },


    /* Control tags with NumPad */

    /* For some reason XK_KP_[0-9] don't work at all
     * Using the following XK codes enables
     * controlling tags regardless of NumLock status
     */

    { MODKEY,                  XK_KP_Insert,   view,           {.ui = ~0} },
    { MODKEY|ShiftMask,        XK_KP_Insert,   tag,            {.ui = ~0} },

    TAGKEYS(                        XK_KP_End,                 0)
    TAGKEYS(                        XK_KP_Down,                1)
    TAGKEYS(                        XK_KP_Next,                2)
    TAGKEYS(                        XK_KP_Left,                3)
    TAGKEYS(                        XK_KP_Begin,               4)
    TAGKEYS(                        XK_KP_Right,               5)
    TAGKEYS(                        XK_KP_Home,                6)
    TAGKEYS(                        XK_KP_Up,                  7)
    TAGKEYS(                        XK_KP_Prior,               8)

    /* Default tag control */

    TAGKEYS(                        XK_1,                      0)
    TAGKEYS(                        XK_2,                      1)
    TAGKEYS(                        XK_3,                      2)
    TAGKEYS(                        XK_4,                      3)
    TAGKEYS(                        XK_5,                      4)
    TAGKEYS(                        XK_6,                      5)
    TAGKEYS(                        XK_7,                      6)
    TAGKEYS(                        XK_8,                      7)
    TAGKEYS(                        XK_9,                      8)

};

/* 1 means resize window by 1 pixel for each scroll event */
static const int scrollsensitivity = 30;

/* resizemousescroll direction argument list */
static const int scrollargs[][2] = {
    /* width change         height change */
    { 0,                    +scrollsensitivity  },
    { 0,                    -scrollsensitivity  },
    { +scrollsensitivity,   0                   },
    { -scrollsensitivity,   0                   },
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static const Button buttons[] = {
    /* click            event mask  button      function            argument */
    { ClkButton,        0,          Button1,    spawn,              SHCMD("~/.config/dwm/wmsh") },
    { ClkLtSymbol,      0,          Button1,    setlayout,          {0} },
    { ClkLtSymbol,      0,          Button3,    setlayout,          {.v = &layouts[2]} },
    { ClkWinTitle,      0,          Button2,    zoom,               {0} },
    { ClkStatusText,    0,          Button2,    spawn,              {.v = termcmd } },
    { ClkClientWin,     MODKEY,     Button1,    movemouse,          {0} },
    { ClkClientWin,     MODKEY,     Button2,    togglefloating,     {0} },
    { ClkClientWin,     MODKEY,     Button3,    resizemouse,        {0} },
    { ClkClientWin,     MODKEY,     Button4,    resizemousescroll,  {.v = &scrollargs[0]} },
    { ClkClientWin,     MODKEY,     Button5,    resizemousescroll,  {.v = &scrollargs[1]} },
    { ClkClientWin,     MODKEY,     Button6,    resizemousescroll,  {.v = &scrollargs[2]} },
    { ClkClientWin,     MODKEY,     Button7,    resizemousescroll,  {.v = &scrollargs[3]} },
    { ClkTagBar,        0,          Button1,    view,               {0} },
    { ClkTagBar,        0,          Button3,    toggleview,         {0} },
    { ClkTagBar,        MODKEY,     Button1,    tag,                {0} },
    { ClkTagBar,        MODKEY,     Button3,    toggletag,          {0} },
};

/* vim:set ft=c: */
