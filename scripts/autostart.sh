#!/bin/sh

sigaudio() {
    until pidof -q dwmblocks; do
        sleep 0.5
    done
    pkill -RTMIN+2 dwmblocks
}

restart() {
    for p in "$@"; do
        pkill "$p"
        setsid -f "$p"
    done
}

xwallpaper --zoom "$(dirname "$0")/wallpaper" &
xautolock -time 25 -locker slock &

restart dwmblocks redshift dunst &
sigaudio &

fcitx5 -r &
# keepassxc &
/lib/polkit-kde-authentication-agent-1 &
picom -b &
